import { SET_CITY, SET_COORDS, SET_PHASE, SET_START, SET_WEATHER, SET_STAGE, HANDLE_ERROR } from '../actions/index';

const INITIAL_STATE = {
  city: null,
  coords: null,
  requestid: 0,
  errorid: 0,
  errortype: null,
  icon: '01d',
  niceday: 'clean', // clean or dark
  partofday: 'day', // day or night
  stage: 'weather', // search or weather
  phase: 'init', // init, ready, loading, error
  start: 'start-none',
};

export default function(state = INITIAL_STATE, action) {
  switch(action.type) {
    case SET_CITY:
      return {...state, city: action.payload, coords: null, requestid: state.requestid+1 }
    case SET_COORDS:
      return {...state, city: action.payload.city, coords: {lat: action.payload.lat, lng: action.payload.lng}, requestid: state.requestid+1 }
    case HANDLE_ERROR:
      return {...state, errortype: action.payload, errorid: state.errorid+1 }
    case SET_PHASE:
      return {...state, phase: action.payload }
    case SET_STAGE:
      return {...state, stage: action.payload }
    case SET_START:
      return {...state, start: action.payload }
    case SET_WEATHER:
      return {...state, ...action.payload }
    default:
      return state;
  }
}
