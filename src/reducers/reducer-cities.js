import { FETCH_CITIES } from '../actions/index';

const INITIAL_STATE = {
  success: null,
  data: null
};

export default function(state = INITIAL_STATE, action) {
  switch(action.type) {
    case FETCH_CITIES:
      if (action.payload.data) {
        return { data: action.payload.data, success: true }
      } else { return { success: false } }
    default:
      return state;
  }
}
