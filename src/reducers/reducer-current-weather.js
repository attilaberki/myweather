import { FETCH_CURRENT_WEATHER } from '../actions/index';

const INITIAL_STATE = {
  success: null,
  data: null,
  requestid: 0,
  error: null,
  errorid: 0,
};

export default function(state = INITIAL_STATE, action) {
  switch(action.type) {
    case FETCH_CURRENT_WEATHER:
      if (action.payload.data) {
        return { ...state, data: action.payload.data, success: true, error: null, requestid: state.requestid+1 }
      } else {
        return { ...state, success: false, error: action.payload.message, errorid: state.errorid+1 };
      }
    default:
      return state;
  }
}
