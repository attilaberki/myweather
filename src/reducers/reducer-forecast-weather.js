import { FETCH_FORECAST_WEATHER } from '../actions/index';

const INITIAL_STATE = {
  success: null,
  data: null,
  requestid: 0,
  erro: null,
  errorid: 0,
};

export default function(state = INITIAL_STATE, action) {
  switch(action.type) {
    case FETCH_FORECAST_WEATHER:
      if (action.payload.data) {
        return { ...state, data: action.payload.data, success: true, error: null, requestid: state.requestid+1 }
      } else {
        return { ...state, success: false, error: action.payload.message, errorid: state.errorid+1 };
      }
    default:
      return state;
  }
}
