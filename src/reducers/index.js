import { combineReducers } from 'redux';
import geoReducer from './reducer-geo';
import citiesReducer from './reducer-cities';
import currentWeatherReducer from './reducer-current-weather';
import forecastWeatherReducer from './reducer-forecast-weather';
import commonReducer from './reducer-common';

const rootReducer = combineReducers({
  geo: geoReducer,
  cities: citiesReducer,
  currentweather: currentWeatherReducer,
  forecastweather: forecastWeatherReducer,
  common: commonReducer,
});

export default rootReducer;
