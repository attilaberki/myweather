import axios from 'axios';

const API_KEY = '84e74c7549e0ce91b889e9c98dc3ece4';
const FORECAST_WEATHER_URL = `http://api.openweathermap.org/data/2.5/forecast/daily?appid=${API_KEY}`;
const CURRENT_WEATHER_URL = `http://api.openweathermap.org/data/2.5/weather?appid=${API_KEY}`;
//const CURRENT_WEATHER_URL = `http://ERROR_api.openweathermap.org/data/2.5/weather?appid=${API_KEY}`;
const CITIES_URL = "api/cities-geonames.json";
//const CITIES_URL = "ERROR_api/cities-geonames.json";
const GEO_URL = "https://freegeoip.net/json/";
//const GEO_URL = "https://ERROR_freegeoip.net/json/";

export const FETCH_GEO = 'FETCH_GEO';
export const FETCH_CITIES = 'FETCH_CITIES';
export const FETCH_CURRENT_WEATHER = 'FETCH_CURRENT_WEATHER';
export const FETCH_FORECAST_WEATHER = 'FETCH_FORECAST_WEATHER';
export const SET_CITY = 'SET_CITY';
export const SET_COORDS = 'SET_COORDS';
export const SET_PHASE = 'SET_PHASE';
export const SET_STAGE = 'SET_STAGE';
export const SET_START = 'SET_START';
export const SET_WEATHER = 'SET_WEATHER';
export const SET_FORECAST  = 'SET_FORECAST';
export const HANDLE_ERROR = 'HANDLE_ERROR';
export const ERROR_TYPE = { none: null, unknown: 'unknown', network: 'network', request: 'request', fewchars: 'fewchars' };

export const STAGE_TYPE = { search: 'search', weather: 'weather' };
export const START_TYPE = { startnone: 'start-none', startsearch: 'start-search', startweather: 'start-weather' };
export const PHASE_TYPE = { init: 'init', ready: 'ready', loading: 'loading', error: 'error' };



export function fetchGeo() {
  const request = axios.get(GEO_URL)
  return ({ type: FETCH_GEO, payload: request });
}

export function fetchCities() {
  const request = axios.get(CITIES_URL)
  return ({ type: FETCH_CITIES, payload: request });
}

export function fetchCurrentWeather(city) {
  const url = `${CURRENT_WEATHER_URL}&q=${city}&units=metric`;
  const request = axios.get(url);
  return ({ type: FETCH_CURRENT_WEATHER, payload: request });
}

export function fetchForecastWeather(city) {
  const url = `${FORECAST_WEATHER_URL}&q=${city}&units=metric&cnt=8`;
  const request = axios.get(url);
  return ({ type: FETCH_FORECAST_WEATHER, payload: request });
}

export function fetchCurrentWeatherCoords(coords) {
  const url = `${CURRENT_WEATHER_URL}&lat=${coords.lat}&lon=${coords.lng}&units=metric`;
  const request = axios.get(url);
  return ({ type: FETCH_CURRENT_WEATHER, payload: request });
}

export function fetchForecastWeatherCoords(coords) {
  const url = `${FORECAST_WEATHER_URL}&lat=${coords.lat}&lon=${coords.lng}&units=metric&cnt=8`;
  const request = axios.get(url);
  return ({ type: FETCH_FORECAST_WEATHER, payload: request });
}

export function setCity(city) {
  return ({ type: SET_CITY, payload: city });
}

export function setCoords(lat, lng, city) {
  const data = {lat, lng, city};
  return ({ type: SET_COORDS, payload: data });
}

export function setPhase(phase) {
  return ({ type: SET_PHASE, payload: phase });
}

export function setStage(stage) {
  return ({ type: SET_STAGE, payload: stage });
}

export function setStart(start) {
  return ({ type: SET_START, payload: start });
}

export function handleError(msg) {
  // 'Network Error', 'Request failed with status code 404', none, Not enough characters
  let errortype = ERROR_TYPE.unknown;
  if (msg.includes('Network')) {errortype = ERROR_TYPE.network;}
  if (msg.includes('Request')) {errortype = ERROR_TYPE.request;}
  if (msg === 'none') {errortype = ERROR_TYPE.none;}
  if (msg === ERROR_TYPE.fewchars) {errortype = ERROR_TYPE.fewchars;}
  return ({ type: HANDLE_ERROR, payload: errortype });
}

export function setWeather(data) {
  const weather = { icon: '01d', niceday: 'clean', partofday: 'day' }
  const cleanicons = ['01d', '02d', '01n', '02n'];

  weather.icon = data.weather[0].icon;
  if (weather.icon.includes('n')) {weather.partofday = 'night'};
  if (!cleanicons.includes(weather.icon)) {weather.niceday = 'dark'};

  return ({ type: SET_WEATHER, payload: weather });
}
