import React, { Component } from 'react';
import { connect } from 'react-redux';
import Autosuggest from 'react-autosuggest';
import AutosuggestHighlightMatch from 'autosuggest-highlight/match';
import AutosuggestHighlightParse from 'autosuggest-highlight/parse';
import IsolatedScroll from 'react-isolated-scroll';
import { Scrollbars } from 'react-custom-scrollbars';
import accents from 'remove-accents';
import numeral from 'numeral';
import classnames from 'classnames';
import { ERROR_TYPE, fetchCities, setCity, setCoords, handleError, setPhase, setStage,
STAGE_TYPE, PHASE_TYPE, START_TYPE } from '../actions/index';

import Icon from './Icon';


const getSuggestions = (value, list) => {
  const inputValue = value.toLowerCase();
  const inputLength = inputValue.length;
  const results = inputLength < 2 ? [] : list.filter(lang =>
    lang.name.toLowerCase().slice(0, inputLength) === inputValue
  );

  if (results.length < 50) { return results; }
  return [];
  //return results;
};

const getSuggestionValue = suggestion => suggestion.name + ', ' +suggestion.country;

const renderSuggestion = (suggestion, { query }) => {
  const suggestionText = `${suggestion.name}, ${suggestion.country}`;
  const matches = AutosuggestHighlightMatch(suggestionText, query);
  const parts = AutosuggestHighlightParse(suggestionText, matches);

  return(
    <div className="sugg-row-info">
      <span className="city">
        {
          parts.map((part, index) => {
            const className = part.highlight ? 'match' : null;
            return <span className={className} key={index}>{part.text}</span>;
          })
        }
      </span>
      <span className="pop">
        lat. {numeral(suggestion.lat).format('0.00')},
        lng. {numeral(suggestion.lng).format('0.00')}
      </span>
    </div>
  )

};


class Form extends Component {

  constructor(props) {
    super(props);
    this.rowheight = 56;
    this.state = {
      city: null,
      cities: null,
      value: '',
      normalvalue: '',
      highlighted: null,
      suggestions: [],
      posindex: 0,
      focusbyinput: false,
    }

  }

  componentWillMount() {
    this.setValue(this.props);
  }


  componentWillReceiveProps(nextProps) {
    if (this.props.common.start !== nextProps.common.start) {
      this.props.fetchCities();
      if (nextProps.common.start === START_TYPE.startsearch) { this.props.setPhase(PHASE_TYPE.loading); }
    }

    if (this.props.cities.success === null && nextProps.cities.success) {
      this.setState({cities: nextProps.cities.data})
      if (nextProps.common.start === START_TYPE.startsearch) { this.props.setPhase(PHASE_TYPE.init); }
    }

    if (this.props.cities.success === null && nextProps.cities.success === false) {
      this.setState({cities: false})
      if (nextProps.common.start === START_TYPE.startsearch) { this.props.setPhase(PHASE_TYPE.init); }
    }

    if (nextProps.common.errortype === null) {
      this.setValue(nextProps);
    }

  }


  componentDidUpdate(prevProps, prevState){
    if (prevProps.common.stage === STAGE_TYPE.weather && this.props.common.stage === STAGE_TYPE.search) {
      if(this.state.focusbyinput) {
        const input = document.getElementById("SearchInput"); input.select();
      }
    }
  }

  setValue(props) {
    if (props.common.city) {
      this.setState({ value: props.common.city })
    }
  }

  onSuggestChange (event, data) {
    let posindex = this.state.posindex;
    if (data.method === 'down') {posindex = (this.state.posindex < (this.state.suggestions.length)) ? this.state.posindex+1 : 0;}
    if (data.method === 'up') {posindex = (this.state.posindex > 0) ? this.state.posindex-1 : this.state.suggestions.length;}
    var scrollContainer = document.getElementById("scrollContainer");
    scrollContainer.scrollTop = (posindex * this.rowheight) - (5*this.rowheight);

    this.setState({ value: data.newValue, posindex });
  };

  onChange (event) {
   this.setState({ value: event.target.value });
  };

  onSuggestionsFetchRequested({ value }) {
   this.setState({ suggestions: getSuggestions(value, this.state.cities) });
  };

  onSuggestionsClearRequested() {
   this.setState({ suggestions: [], posindex: 0 });
  };

  onSuggestionHighlighted(data) {
    if (data.suggestion) {
      this.setState({highlighted: data.suggestion})
    }
  }

  renderSuggestionsContainer({ containerProps , children, query }) {
    return (
      <div className="suggestions-wrapper">
        <div className="suggestions-container">
          <Scrollbars
            {... containerProps}
            autoHeight
            autoHeightMin={0}
            autoHeightMax={5*this.rowheight}
            style={{ width: 500 }}
            className="suggestions"
            renderView={props => <div {...props} id="scrollContainer"/>}
            renderThumbVertical={props => <div {...props} className="thumb-vertical"/>}
            >
              {children}
          </Scrollbars>
        </div>
      </div>
    );
  }



  onSubmit(event) {
    event.preventDefault();

    if (this.state.value.length > 2) {
      const highlightedvalue = (this.state.highlighted) ? `${this.state.highlighted.name}, ${this.state.highlighted.country}` : null;
      if (highlightedvalue === this.state.value) {
        this.props.setCoords(this.state.highlighted.lat, this.state.highlighted.lng, `${this.state.highlighted.name}, ${this.state.highlighted.country}`);
      } else {
        this.props.setCity(this.state.value);
      }
    } else {
      this.props.handleError(ERROR_TYPE.fewchars);
    }
  }

  onFocus(e) {
    this.setState({focusbyinput: true});
    this.props.setStage(STAGE_TYPE.search)
  }

  clickLoupe() {
    if (this.props.common.stage === STAGE_TYPE.weather) {
      this.setState({focusbyinput: false});
      this.props.setStage(STAGE_TYPE.search)
    }
  }


  renderInput() {
    const { value, suggestions } = this.state;
    const inputProps = {
      placeholder: 'Minimum 3 characters',
      size: 2,
      id: 'SearchInput',
      className: classnames({smalltext: value.length > 20}),
      value, onChange: this.onSuggestChange.bind(this),
      onFocus: this.onFocus.bind(this)
    };

    if (this.state.cities) {
      return (
        <div className={classnames('suggestcont')}>
          <Autosuggest
            suggestions={suggestions}
            renderSuggestionsContainer={this.renderSuggestionsContainer.bind(this)}
            onSuggestionsFetchRequested={this.onSuggestionsFetchRequested.bind(this)}
            onSuggestionsClearRequested={this.onSuggestionsClearRequested.bind(this)}
            getSuggestionValue={getSuggestionValue.bind(this)}
            onSuggestionHighlighted={this.onSuggestionHighlighted.bind(this)}
            renderSuggestion={renderSuggestion.bind(this)}
            inputProps={inputProps}
          />
        </div>
      );
    }

    return (
      <input type="text" placeholder="Minimum 3 characters" id="SearchInput" size="2" onChange={this.onChange.bind(this)} onFocus={this.onFocus.bind(this)} value={this.state.value}/>
    );
  }

  renderButton() {
    return <button type="submit"><span>Search</span></button>;
  }

  render() {
    if (this.state.cities !== null || this.props.common.start === START_TYPE.startweather) {
      return (
        <div className={classnames('form-container', this.props.common.start, this.props.common.stage, this.props.common.phase)}>
          <form onSubmit={this.onSubmit.bind(this)}>
            <div className="input-box">
              <div className="input-elements">
                <label>enter your location</label>
                <Icon name="search" onClick={this.clickLoupe.bind(this)}/>
                {this.renderInput()}
                <div className="underline"></div>
                <div className={classnames('shadowtext', {smalltext: this.state.value.length > 20})}>{this.state.value}</div>
              </div>

            </div>
            <div className="button-box">
              {this.renderButton()}
            </div>
          </form>
        </div>
      )
    }

    return null;
  };
}


function mapStateToProps({cities, common, geo}) {
  return {cities, common, geo};
}

export default connect(mapStateToProps, { fetchCities, setCity, setCoords, setPhase, setStage, handleError })(Form);
