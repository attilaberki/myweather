import React, { Component } from 'react';
import { connect } from 'react-redux';
import classnames from 'classnames';

import Icon from './Icon';

class Background extends Component {
  render() {
    return (
      <div className={classnames('bg-elements')}>
        <div className={classnames('bg-colors', this.props.common.start, this.props.common.stage, this.props.common.phase, this.props.common.partofday, this.props.common.niceday)}>
          <div className="color day-clean"></div>
          <div className="color day-dark"></div>
          <div className="color night-clean"></div>
          <div className="color night-dark"></div>
        </div>
        <div className={classnames('loader-spinner', this.props.common.stage, this.props.common.phase)}><div className="dot1"></div><div className="dot2"></div></div>
        <Icon name="logo" className={classnames('main-logo', this.props.common.start, this.props.common.stage, this.props.common.phase)} />
      </div>
    )
  }
}


function mapStateToProps({common}) {
  return {common};
}

export default connect(mapStateToProps)(Background);
