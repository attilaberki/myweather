import React, { Component } from 'react';
import { connect } from 'react-redux';
import classnames from 'classnames';

import { ERROR_TYPE } from '../actions/index';


class Feedback extends Component {

  createMessage() {
    const {errortype} = this.props.common;
    const message = {title: null, text: null}

    switch (errortype) {
      case ERROR_TYPE.unknown:
        message.title = "Congratulation :)";
        message.text = "You discovered an unhandled problem";
        break;
      case ERROR_TYPE.network:
        message.title = "Network problem!";
        message.text = "Our server hasn't responded or maybe you have internet problems";
        break;
      case ERROR_TYPE.request:
        message.title = "Your location not found :(";
        message.text = "The Open Weather Map didn't find your request";
        break;
      case ERROR_TYPE.fewchars:
        message.title = "Text is too short";
        message.text = "Please type minimum 3 characters to the input field";
        break;
      default:
        message.title = null;
        message.text = null;
        break;

    }

    return message;
  }

  render() {
    const message = this.createMessage()

    return (
      <div className={classnames('feedback-elements')}>
        <div className={classnames('feedback-box', this.props.common.stage, this.props.common.phase)}>
          <h3 key={`feedback-title-${this.props.common.errorid}`}>{message.title}</h3>
          <p key={`feedback-text-${this.props.common.errorid}`}>{message.text}</p>
        </div>
      </div>
    )
  }
}


function mapStateToProps({common}) {
  return {common};
}

export default connect(mapStateToProps)(Feedback);
