import React, { Component } from 'react';
import { connect } from 'react-redux';
import dateFormat from 'dateformat';
import classnames from 'classnames';

import Icon from './Icon';

class Weather extends Component {
  render() {
    const {currentweather, common} = this.props;

    if (currentweather.success) {
      const {main, wind, weather} = currentweather.data;
      const now = new Date(currentweather.data.dt*1000);

      return (
        <div className={classnames('weather-elements', common.start, common.stage)}>
          <div className="temp">
            <span className="num">{Math.round(main.temp)}</span>
            <span className="sign">°</span>
          </div>

          <table>
            <tbody>
            <tr>
              <td className="cell date">{dateFormat(now, "mmmm dS")}</td>
              <td className="cell text">
                {dateFormat(now, "dddd")}
              </td>
            </tr>
            <tr>
              <td className="cell text minmax" colSpan="2">
                Min: {Math.round(main.temp_min)}°
                <span></span>
                Max: {Math.round(main.temp_max)}°
              </td>
            </tr>
            <tr>
              <td className="cell title">Wind</td>
              <td className="cell text">{wind.speed} m/s</td>
            </tr>
            <tr>
              <td className="cell title">Cloudiness</td>
              <td className="cell text cloudiness">{weather[0].description}</td>
            </tr>
            <tr>
              <td className="cell title">Pressure</td>
              <td className="cell text">{main.pressure} hpa</td>
            </tr>
            <tr>
              <td className="cell title">Humidity</td>
              <td className="cell text">{main.humidity} %</td>
            </tr>
            </tbody>
          </table>
        </div>
      )
    }

    return null;
  }
}


function mapStateToProps({common, currentweather}) {
  return {common, currentweather};
}

export default connect(mapStateToProps)(Weather);
