import React, { Component } from 'react';
import { connect } from 'react-redux';
import dateFormat from 'dateformat';
import classnames from 'classnames';

import Icon from './Icon';

class Forecast extends Component {

  forecastItem(data, index) {
    const now = new Date(data.dt*1000);
    return (
      <div className="item" key={`item-${index}`}>
        <div className="first">
          <div className="data date">{dateFormat(now, "mmm d.")}</div>
          <div className="data illust"><Icon name={data.weather[0].icon} /></div>
        </div>
        <div className="second">
          <div className="data info desc"><span>{data.weather[0].description}</span></div>
          <div className="data info min">Min: <span>{Math.round(data.temp.min)}°</span></div>
          <div className="data info max">Max: <span>{Math.round(data.temp.max)}°</span></div>
        </div>
      </div>
    )
  }

  renderItems() {
    const {forecastweather, common} = this.props;

    if (forecastweather.success) {
      const {list} = forecastweather.data;
      return list.map((item, index) => {
        if (index > 0) { //0 is today
          return this.forecastItem(item, index);
        }
      })
    }
    return null;
  }

  render() {
    const {forecastweather, common} = this.props;

    if (forecastweather.success) {
      return (
        <div className={classnames('forecast-elements', common.start, common.stage)}>
          <div className="forecast-wrapper">
            <div className="forecast-container">
              {this.renderItems()}
            </div>
          </div>
        </div>
      )
    }

    return null;
  }
}


function mapStateToProps({common, forecastweather}) {
  return {common, forecastweather};
}

export default connect(mapStateToProps)(Forecast);
