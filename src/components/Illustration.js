import React, { Component } from 'react';
import { connect } from 'react-redux';
import classnames from 'classnames';

import Icon from './Icon';

class Illustration extends Component {
  render() {
    const {currentweather, common} = this.props;

    if (currentweather.success) {
      return (
        <div className={classnames('illustration-elements')}>
          <div className={classnames('icon-container', common.start, common.stage)}>
            <Icon name={common.icon} className={classnames('weather-icon')} />
          </div>
        </div>
      )
    }

    return null;
  }
}


function mapStateToProps({common, currentweather}) {
  return {common, currentweather};
}

export default connect(mapStateToProps)(Illustration);
