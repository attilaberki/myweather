import React, { Component } from 'react';
import classnames from 'classnames';
import SVGInline from "react-svg-inline";

import logoSVG from "../assets/logo.svg";
import searchSVG from "../assets/search.svg";

import d01SVG from "../assets/01d.svg";
import d02SVG from "../assets/02d.svg";
import d03SVG from "../assets/03d.svg";
import d04SVG from "../assets/04d.svg";
import d09SVG from "../assets/09d.svg";
import d10SVG from "../assets/10d.svg";
import d11SVG from "../assets/11d.svg";
import d13SVG from "../assets/13d.svg";
import d50SVG from "../assets/50d.svg";

import n01SVG from "../assets/01n.svg";
import n02SVG from "../assets/02n.svg";
import n10SVG from "../assets/10n.svg";



class Icon extends Component {

  renderIcon() {

    if (this.props.name === 'logo') { return <SVGInline svg={ logoSVG } />; }
    if (this.props.name === 'search') { return <SVGInline svg={ searchSVG } />; }

    if (this.props.name === '01d') { return <SVGInline svg={ d01SVG } />; }
    if (this.props.name === '01n') { return <SVGInline svg={ n01SVG } />; }
    if (this.props.name === '02d') { return <SVGInline svg={ d02SVG } />; }
    if (this.props.name === '02n') { return <SVGInline svg={ n02SVG } />; }
    if (this.props.name === '03d' || this.props.name === '03n') { return <SVGInline svg={ d03SVG } />; }
    if (this.props.name === '04d' || this.props.name === '04n') { return <SVGInline svg={ d04SVG } />; }
    if (this.props.name === '09d' || this.props.name === '09n') { return <SVGInline svg={ d09SVG } />; }
    if (this.props.name === '10d') { return <SVGInline svg={ d10SVG } />; }
    if (this.props.name === '10n') { return <SVGInline svg={ n10SVG } />; }
    if (this.props.name === '11d' || this.props.name === '11n') { return <SVGInline svg={ d11SVG } />; }
    if (this.props.name === '13d' || this.props.name === '13n') { return <SVGInline svg={ d13SVG } />; }
    if (this.props.name === '50d' || this.props.name === '50n') { return <SVGInline svg={ d50SVG } />; }

    return <span>SVG NOT FOUND</span>
  }

  renderLabel() {
    if(this.props.debug) {
      return <label>{this.props.name}</label>
    }

    return null;
  }

  render() {
    return (
      <span
        onClick={this.props.onClick}
        className={classnames('svg-icon', this.props.name, this.props.className)}
      >
          {this.renderIcon()}{this.renderLabel()}
      </span>
    )
  }
}

export default Icon;
