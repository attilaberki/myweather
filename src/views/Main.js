import React, { Component } from 'react';
import { connect } from 'react-redux';
import classnames from 'classnames';
import {
  fetchGeo, fetchCurrentWeather, fetchForecastWeather, fetchCurrentWeatherCoords, fetchForecastWeatherCoords,
  setCity, setCoords, handleError, setPhase, setStage, setStart, setWeather,
  STAGE_TYPE, PHASE_TYPE, START_TYPE
} from '../actions/index';

import Form from '../components/Form';
import Feedback from '../components/Feedback';
import Background from '../components/Background';
import Weather from '../components/Weather';
import Illustration from '../components/Illustration';
import Forecast from '../components/Forecast';


class Main extends Component {

  componentWillMount() {
    this.props.setPhase(PHASE_TYPE.init);
    this.props.fetchGeo();
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.geo.success === null && nextProps.geo.success) {
      this.props.setCoords(nextProps.geo.data.latitude, nextProps.geo.data.longitude, `${nextProps.geo.data.city}, ${nextProps.geo.data.country_code}`);
    }

    if (this.props.geo.success === null && nextProps.geo.success === false) {
      this.props.setStage(STAGE_TYPE.search);
    }

    if (this.props.common.requestid < nextProps.common.requestid) {
      this.props.setPhase(PHASE_TYPE.loading);
      this.newRequest(nextProps.common);
    }

    if (this.props.currentweather.requestid < nextProps.currentweather.requestid) {
      this.props.setPhase(PHASE_TYPE.ready);
      this.props.setStage(STAGE_TYPE.weather);
      this.props.handleError('none');
      this.props.setWeather(nextProps.currentweather.data);
      try {
        const input = document.getElementById("SearchInput"); input.blur();
      } catch (e) {}

    }

    if (this.props.currentweather.errorid < nextProps.currentweather.errorid) {
      if (!nextProps.currentweather.success) {
        this.props.handleError(nextProps.currentweather.error);
      }
    }

    if (this.props.common.errorid < nextProps.common.errorid) {
      if (nextProps.common.errortype) {
        this.props.setPhase(PHASE_TYPE.error);
      }
    }


  }

  componentDidUpdate(prevProps, prevState){
    if (prevProps.geo.success === null && this.props.geo.success === false) {
      this.props.setStart(START_TYPE.startsearch);
    }

    if (prevProps.geo.success === null && this.props.geo.success === true) {
      this.props.setStart(START_TYPE.startweather);
    }

    if (prevProps.common.requestid < this.props.common.requestid) {
      if (prevProps.common.start === START_TYPE.startsearch) {
        this.props.setStart(START_TYPE.startnone);
      } else if (this.props.common.requestid > 1) {
        this.props.setStart(START_TYPE.startnone);
      }

    }
  }

  newRequest(data) {
    if (data.coords) {
      this.props.fetchCurrentWeatherCoords(data.coords);
      this.props.fetchForecastWeatherCoords(data.coords);
    } else if(data.city) {
      this.props.fetchCurrentWeather(data.city);
      this.props.fetchForecastWeather(data.city);
    }
  }

  render() {
    return (
      <div className={classnames('app-container', this.props.common.start, this.props.common.stage, this.props.common.phase)}>
        <div className="wrapper">
          <Background />
          <Feedback />
          <Illustration />
          <Forecast />
          <Weather />
          <Form />
        </div>
      </div>
    );
  }
}


function mapStateToProps({geo, currentweather, forecastweather, common}){
  return {geo, currentweather, forecastweather, common};
}

export default connect(mapStateToProps, { fetchGeo, fetchCurrentWeather, fetchForecastWeather, fetchCurrentWeatherCoords, fetchForecastWeatherCoords,
  setCity, setCoords, handleError, setPhase, setStage, setStart, setWeather })(Main);
