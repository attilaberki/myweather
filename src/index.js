import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import ReduxPromise from 'redux-promise';

import '../scss/style.scss';

import reducers from './reducers';
import Main from './views/Main';

const createStoreWithMiddleware = applyMiddleware(ReduxPromise)(createStore);

const App = () => {
  return (
    <Provider store={createStoreWithMiddleware(reducers)}>
      <Main />
    </Provider>
  );
};

ReactDOM.render(<App />, document.getElementById('app'));
